
def asdf(time, pm)
  if pm > 0
    time = (time * 2.0).round / 2.0 - 0.5
  else
    time = (time * 2.0).round / 2.0
  end
  #puts "zw: #{time}"
  time += pm
end

MINUTE_BORDER = 30 * 60 * 1000

def up(t)
  # 50 div 1 mod 20 up 70 down 30
  a = t % MINUTE_BORDER
  a == 0 ? t + MINUTE_BORDER : t + (MINUTE_BORDER - a)
end

def down(t)
  a = t % MINUTE_BORDER
  a == 0 ? t - MINUTE_BORDER : t - a
end

def time_conversion(minutes)
    minutes = minutes / 60 / 1000
    hours = minutes / 60
    rest = minutes  % 60
    "#{hours.to_s.rjust(2, "0")}:#{rest.to_s.rjust(2, "0")}"
end

# Zeit steht bei 1:45 (Stunden). Man drückt einmal "+" und Zeit springt auf 2:00 (Stunden).
# Drückt man "-", so springt die Zeit auf 1:30 (Stunden).
# Oder bei Restzeit 1:01 (Stunden), bei "+" wirds 1:30 (Stunden),
# bei "-" 1:00 (Stunden). Bei
# 0:29 (Stunden) und Drücken auf "-" geht das Gerät dann logischerweise aus.
times = [
  (60*5 + 3) * 60 * 1000,
  (60+60+5) * 60 * 1000, # 2:05
  (60+59) * 60 * 1000,
  (60+45) * 60 * 1000, # 1:45
  (60+15) * 60 * 1000, # 1:15
  (60+2) * 60 * 1000,
  (60) * 60 * 1000,
  (50) * 60 * 1000,
  (45) * 60 * 1000,
  (30) * 60 * 1000,
  (15) * 60 * 1000,
  ( 1) * 60 * 1000
]

times.each do |t|

  puts "#{t}ms div #{t / 30} mod #{t % 30}"
  puts "#{time_conversion(t)} up #{time_conversion(up(t))} down #{time_conversion(down(t))}"

end

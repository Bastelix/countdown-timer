
#ifndef _TIMER_H_
#define _TIMER_H_

// minutes border for up/down counter
// we want to round up/down to 30 minutes so 1:45 up = 2:00 and down = 1:30
//const unsigned long MINUTE_BORDER = 30L * 60L * 1000L;
const unsigned long MINUTE_BORDER = 1800000L;

/**
 * count up to the next MINUTE_BORDER minutes
 */
unsigned long timer_up(const unsigned long t);

/**
 * count down to the next MINUTE_BORDER minutes
 */
unsigned long timer_down(const unsigned long t);

unsigned int hours_of_ms(const unsigned long t);

unsigned int minutes_of_ms(const unsigned long t);

#endif

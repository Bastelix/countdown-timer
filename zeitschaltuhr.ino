/**
 * TODO description of this source file
 *
 * @license GNU GPLv3
 *
 * Copyright (c) 2019 bastelix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <TM1637Display.h>
#include "timer.h"

// Module connection pins (Digital Pins)
const uint8_t CLK = 4;
const uint8_t DIO = 5;

const uint8_t KEY_TIME_UP    = 6; // 6 PCINT22 - Port D
const uint8_t KEY_TIME_DOWN  = 7; // 7 PCINT23 - Port D
const uint8_t KEY_TIME_START = 8; // 8 PCINT0  - Port B
const uint8_t KEY_TIME_STOP  = 9; // 9 PCINT1  - Port B

// INT 0 2
// INT 1 3
const uint8_t ERROR_IRQ_PIN  = 2; // 2 INT0
const uint8_t SWITCH_PIN = 10;

// The amount of time (in milliseconds) between tests
#define TEST_DELAY   2000

// The logic level for a pressed down button
#define BUTTON_LEVEL LOW

#define SWITCH_ON  HIGH
#define SWITCH_OFF LOW

#define IRQ_ERROR_LEVEL  HIGH
#define IRQ_OK_LEVEL LOW

/**
 * target_time : the count down time holder
 */
volatile unsigned long target_time = 0;
/**
 * run : set to 1 if the countdown timer is running; 0 otherwise
 */
volatile uint8_t run = 0;

//const unsigned long TARGET_TIME_MAX = 24 * 60 * 60 * 1000; // 24h in ms =  86400000
const unsigned long TARGET_TIME_MAX = 86400000;

const uint8_t SEG_DONE[] = {
  SEG_B | SEG_C | SEG_D | SEG_E | SEG_G,           // d
  SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F,   // O
  SEG_C | SEG_E | SEG_G,                           // n
  SEG_A | SEG_D | SEG_E | SEG_F | SEG_G            // E
  };

/**
 *
 */
TM1637Display display(CLK, DIO);

void setup() {
  Serial.begin(9600);
  Serial.println("======== zeitschaltuhr.ino");

  pinMode(SWITCH_PIN, OUTPUT);
  digitalWrite(SWITCH_PIN, SWITCH_OFF);

  // TODO IRQ ISR
  pinMode(ERROR_IRQ_PIN, INPUT);

  // configure and test display
  display.setBrightness(0x0f);
  // All segments on
  uint8_t data[] = { 0xff, 0xff, 0xff, 0xff };
  display.setSegments(data);
  delay(TEST_DELAY);

  // write 0:01 to the display for test purpose
  data[3] = 0b00111111;
  data[3] = display.encodeDigit(1);
  data[2] = 0b00111111;
  data[1] = 0b10111111;
  data[0] = 0x00;
  display.setSegments(data);
  delay(TEST_DELAY);

  // enable PCINT IRQ for buttons
  cli();
  PCICR  |= 0b00000101; // Enables Ports B and D Pin Change Interrupts
  PCMSK0 |= 0b00000011; // PCINT0 & PCINT1
  PCMSK2 |= 0b11000000; // PCINT22 & PCINT23
  sei();
}

void loop() {
  if (run) {
    unsigned long diff = target_time - millis();

    if (diff > target_time || diff == 0) {
      digitalWrite(SWITCH_PIN, SWITCH_OFF);
      run = 0;
      target_time = 0;
      diff = 0;
    } else {
      digitalWrite(SWITCH_PIN, SWITCH_ON);
    }
    update_display(diff);
  } else {
    update_display(target_time);
  }

  delay(100); // TODO das delay kann eigentlich weg
}

/**
 * @param time the time to show in the display
 */
void update_display(unsigned long time) {
  uint8_t hours = (uint8_t) hours_of_ms(time);
  uint8_t minutes = (uint8_t) minutes_of_ms(time);

  uint8_t data[] = {
    hours > 10 ? display.encodeDigit(hours / 10) : 0,
    display.encodeDigit(hours % 10) | SEG_DP, // doppelpunkt anzeigen
    display.encodeDigit(minutes / 10),
    display.encodeDigit(minutes % 10)
  };
  display.setSegments(data);
}

/**
 * PCINT0 pin change interrupt handler on port B for start and stop buttons
 */
ISR(PCINT0_vect) {
  if (digitalRead(KEY_TIME_STOP) == BUTTON_LEVEL) {
    digitalWrite(SWITCH_PIN, SWITCH_OFF);
    run = 0;
    target_time = 0;
  }
  if (digitalRead(KEY_TIME_START) == BUTTON_LEVEL && target_time > 0 && !run) {
    target_time += millis();
    digitalWrite(SWITCH_PIN, SWITCH_ON);
    run = 1;
  }
}

/**
 * PCINT2 pin change interrupt handler on port D for up and down buttons
 */
ISR(PCINT2_vect) {
  if (digitalRead(KEY_TIME_UP) == BUTTON_LEVEL && timer_up(target_time) < TARGET_TIME_MAX) {
    if (run) {
      unsigned long diff = target_time - millis() + 1000;
      target_time = millis() + timer_up(diff);
    } else {
      target_time = timer_up(target_time);
    }
  }
  if (digitalRead(KEY_TIME_DOWN) == BUTTON_LEVEL && target_time > 0 && timer_down(target_time) >= 0) {
    if (run) {
      unsigned long diff = target_time - millis();
      target_time = timer_down(diff);
    } else {
      target_time = timer_down(target_time);
    }
  }

  delay(200); // FIXME das ist keine gute lösung zum entprellen
}

#include "timer.h"

#include <Arduino.h>

unsigned long timer_up(const unsigned long t) {
  unsigned long a = t % MINUTE_BORDER;
  /*Serial.println("timer_up");
  Serial.print("MINUTE_BORDER ");
  Serial.println(MINUTE_BORDER);
  Serial.print("t=");
  Serial.println(t);
  Serial.print("a=");
  Serial.println(a);
  Serial.println(a == 0 ? t + MINUTE_BORDER : t + (MINUTE_BORDER - a));
  Serial.println("###################"); */
  return a == 0 ? t + MINUTE_BORDER : t + (MINUTE_BORDER - a);
}

unsigned long timer_down(const unsigned long t) {
  unsigned long a = t % MINUTE_BORDER;
  /*Serial.println("timer_down");
  Serial.print("MINUTE_BORDER ");
  Serial.println(MINUTE_BORDER);
  Serial.print("t=");
  Serial.println(t);
  Serial.print("a=");
  Serial.println(a);
  Serial.println(a == 0 ? t - MINUTE_BORDER : t - a);
  Serial.println("###################");*/
  return a == 0 ? t - MINUTE_BORDER : t - a;
}

unsigned int hours_of_ms(const unsigned long t) {
  return (t / 60 / 1000) / 60;
}

unsigned int minutes_of_ms(const unsigned long t) {
  return (t / 60 / 1000)  % 60;
}
